const Express = require("express");

const myData = require("./data.json");

const app = Express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

const PORT = 3001;

app.get("/roi-results", (req, res) => {
  res.send(myData);
});

app.post("/roi-results", (req, res) => {
  res.send(myData);
});

app.listen(PORT, () => console.log(`listening on port  ${PORT}`));
